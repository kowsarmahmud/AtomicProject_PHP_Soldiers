<?php
//include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'AtomicProject_PHP_Soldiers' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php'); //using absolute path

defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
defined('SITE_ROOT') ? null : define('SITE_ROOT', 'C:' . DS . 'xampp' . DS . 'htdocs' . DS . 'AtomicProject_PHP_Soldiers');
require_once(SITE_ROOT . DS . "vendor/autoload.php");

use App\Bitm\PHP_Soldiers\Checkbox\Hobby;

$hobby = new Hobby();
//echo "<br>";
//$hobby->index();
// $hobby->create();
?>  

<?php include 'layout/header.php'; ?>

<!-- start: Content -->
<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.php">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Forms</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Favourite Hobby</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form action="store.php" method="post" class="form-horizontal">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="title"> Name</label>
                            <div class="controls">
                                <input class="input-xlarge focused"
                                       autofocus="autofocus" 
                                       id="title" 
                                       type="text" 
                                       value=""
                                       name="title"
                                       tabindex="1"
                                       placeholder="input name"
                                       required="required" >
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Your Favourite Hobby</label>
                            <div class="controls">
                                <label class="checkbox inline">
                                    <input type="checkbox" name="cricket" id="inlineCheckbox1" value="Cricket"> Cricket
                                </label>
                                <label class="checkbox inline">
                                    <input type="checkbox" name="football" id="inlineCheckbox2" value="Football"> Football
                                </label>
                                <label class="checkbox inline">
                                    <input type="checkbox" name="tanis" id="inlineCheckbox3" value="Tanis"> Tanis
                                </label>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" tabindex="2" class="btn btn-primary">Save</button>
                            <button type="submit" tabindex="3" class="btn btn-primary">Save & Add Again</button>
                            <input tabindex="3" tabindex="4" class="btn" type="reset" value="Reset" />
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->
        <a class="btn btn-success" href="index.php">Go to List</a>
        <a class="btn btn-success" href="javascript:history.go(-1)">Back</a>
        <h1><?php //$hobby->create();   ?></h1>

    </div><!--/row-->
</div><!--/.fluid-container-->
<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->

<?php include 'layout/footer.php'; ?>
