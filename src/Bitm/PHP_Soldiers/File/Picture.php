<?php

namespace App\Bitm\PHP_Soldiers\File;
use App\BITM\PHP_Soldiers\Utility\Utility;

class Picture {

    public $table = "profilepicture";
    public $id;
    public $name;
    public $filetmp;
    public $filename;
    public $filetype;
    public $paths;
    public $connection;

    public function __construct($post = false, $get = false, $files = false) {
        $this->id = $get["id"];
        $this->name = $post["floName"];
        $this->filetmp = $files["imgName"]["tmp_name"];
        $this->filename = $files["imgName"]["name"];
        $this->filetype = $files["imgName"]["type"];
        $this->paths = "Uploads/" . $this->filename;
        move_uploaded_file($this->filetmp, $this->paths);
        $this->open_connection();
    }

    public function store() {
        
        $sql = "INSERT INTO ".$this->table." (name,paths)VALUES('{$this->name}','{$this->paths}')";
        $query = mysql_query($sql);
        if ($query) {
            $message = "<p class='success'>Data Insert Successfully.</p>";
            header('Location:index.php');
        } else {
            $message = "<p class='errors'>Data Insert Failed.</p>";
            // Utility::message($message);
        }
        // Utility::redirect($_SERVER["HTTP_REFERER"]);
    }

    public function open_connection() {
        $this->connection = mysql_connect("localhost", "root", "");
        if (!$this->connection) {
            die("Database connection failed: " . mysql_error());
        } else {
            $db_select = mysql_select_db("atomicproject", $this->connection);
            if (!$db_select) {
                die("Database selection failed: " . mysql_error());
            }
        }
    }

    public function close_connection() {
        if (isset($this->connection)) {
            mysql_close($this->connection);
            unset($this->connection);
        }
    }

    public function index() {

        $hobbys = array();

        $query = "SELECT * FROM " . $this->table . " ORDER BY id DESC";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $hobbys[] = $row;
        }
        return $hobbys;
    }


    public function get_single_id_details($id = NULL) {

        $query = "SELECT * FROM " . $this->table . " WHERE id='$id' ";
        $result = mysql_query($query);

        return $result;
    }

    public function update_single_id_details() {

        $query = "UPDATE " . $this->table . " SET `title` = '" . $this->title . "' WHERE `" . $this->table . "`.`id` = " . $this->id . "";
        if (mysql_query($query)) {
            return true;
        } else {
            die('Query problem' . mysql_error());
        }
    }

    public function delete($id = NULL) {

        $query = "DELETE FROM " . $this->table . " WHERE `" . $this->table . "`.`id` = '$id'";

        if (mysql_query($query)) {
            header('Location:index.php');
        } else {
            die('Query problem' . mysql_error());
        }
    }

    public function published($id = NULL) {

        $quary = "UPDATE `" . $this->table . "` SET `deletion_status` = '1' WHERE `id` = '$id .'";
        mysql_query($quary);
        header('Location:index.php');
    }

    public function unpublished($id = NULL) {

        $quary = "UPDATE `" . $this->table . "` SET `deletion_status` = '0' WHERE `id` = '$id .'";
        mysql_query($quary);
        header('Location:index.php');
    }

}
