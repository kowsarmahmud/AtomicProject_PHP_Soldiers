<?php

namespace App\Bitm\PHP_Soldiers\Checkbox;
use App\BITM\PHP_Soldiers\Utility\Utility;

class Hobby {

    public $table = "hobby";
    public $id;
    public $title;
    public $cricket;
    public $football;
    public $tanis;
    public $connection;

    public function __construct($data = false) {
        $this->id = $data['id'];
        $this->title = $data['title'];
        $this->cricket = $data['cricket'];
        $this->football = $data['football'];
        $this->tanis = $data['tanis'];

        $this->open_connection();
    }

    public function open_connection() {
        $this->connection = mysql_connect("localhost", "root", "");
        if (!$this->connection) {
            die("Database connection failed: " . mysql_error());
        } else {
            $db_select = mysql_select_db("atomicproject", $this->connection);
            if (!$db_select) {
                die("Database selection failed: " . mysql_error());
            }
        }
    }

    public function close_connection() {
        if (isset($this->connection)) {
            mysql_close($this->connection);
            unset($this->connection);
        }
    }

    public function index() {

        $hobbys = array();

        $query = "SELECT * FROM " . $this->table . " ORDER BY id DESC";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $hobbys[] = $row;
        }
        return $hobbys;
    }

    public function store() {

        $query = "INSERT INTO " . $this->table . " ( `title`,`cricket`,`football`,`tanis`) VALUES ( '" . $this->title . "','" . $this->cricket . "','" . $this->football . "','" . $this->tanis . "')";
        $result = mysql_query($query);

        if ($result) {
            $message = "<h2>Hobby title is added successfully.</h2>";
            Utility::message($message);
        } else {
            $message = "<h2>There is an error while saving data. Please try again later.</h2>";
            Utility::message($message);
        }
    }

    public function get_single_id_details($id = NULL) {

        $query = "SELECT * FROM " . $this->table . " WHERE id='$id' ";
        $result = mysql_query($query);

        return $result;
    }

    public function update_single_id_details() {

        $query = "UPDATE " . $this->table . " SET `title` = '" . $this->title . "' WHERE `" . $this->table . "`.`id` = " . $this->id . "";
        if (mysql_query($query)) {
            $message = "<h2>Hobby is updated successfully.</h2>";
            Utility::message($message);
            return true;
        } else {
            die('Query problem' . mysql_error());
        }
    }

    public function delete($id = NULL) {

        $query = "DELETE FROM " . $this->table . " WHERE `" . $this->table . "`.`id` = '$id'";

        if (mysql_query($query)) {
            $message = "<h2>Hobby delete is added successfully.</h2>";
            Utility::message($message);
            header('Location:index.php');
        } else {
            die('Query problem' . mysql_error());
        }
    }

    public function published($id = NULL) {

        $quary = "UPDATE `" . $this->table . "` SET `deletion_status` = '1' WHERE `id` = '$id .'";
        mysql_query($quary);
        header('Location:index.php');
    }

    public function unpublished($id = NULL) {

        $quary = "UPDATE `" . $this->table . "` SET `deletion_status` = '0' WHERE `id` = '$id .'";
        mysql_query($quary);
        header('Location:index.php');
    }

}
