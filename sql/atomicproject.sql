-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2016 at 09:55 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `deletion_status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `date`, `deletion_status`) VALUES
(18, 'Kowsar Mahmud', '1990-11-08', 0),
(22, 'Oliur Rahaman', '1992-11-08', 0),
(23, 'Soikot Bai', '1952-12-19', 0),
(24, 'Shahin Bai', '1990-11-22', 0);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `author` varchar(11) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `name`, `author`, `deletion_status`) VALUES
(14, 'Social Science', 'M A Salam', 0),
(15, 'Islamic History', 'M A Salam', 0),
(16, 'Political Science', 'M A Salam', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bookmark`
--

CREATE TABLE IF NOT EXISTS `bookmark` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookmark`
--

INSERT INTO `bookmark` (`id`, `name`, `url`) VALUES
(2, 'yahoo', 'http://www.yahoo.com'),
(3, 'google.com', 'http://www.yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `citys`
--

CREATE TABLE IF NOT EXISTS `citys` (
`id` int(11) NOT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `citys`
--

INSERT INTO `citys` (`id`, `country`, `city`) VALUES
(4, 'Bangladesh1', 'Rajshahi'),
(7, 'Bangladesh', 'Gazipur'),
(8, 'Bangladesh', 'Rajshahi'),
(10, 'bangladesh', 'Gazipur');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE IF NOT EXISTS `emails` (
`id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `email`) VALUES
(15, 'mahmud7846@gmail.com'),
(16, 'oliur@gmail.com'),
(17, 'soikot@gmail.com'),
(18, 'shahin@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE IF NOT EXISTS `hobby` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `deletion_status` tinyint(1) NOT NULL DEFAULT '1',
  `cricket` varchar(255) NOT NULL,
  `football` varchar(255) NOT NULL,
  `tanis` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `title`, `deletion_status`, `cricket`, `football`, `tanis`) VALUES
(87, 'Md. Kowsar Mahmud', 0, 'Cricket', 'Football', 'Tanis'),
(101, 'Md. Oliur Rahaman', 0, 'Cricket', 'Football', 'Tanis'),
(121, 'Soikot Bai', 1, 'Cricket', 'Football', 'Tanis'),
(134, ' 	Shahin Bai', 1, '', 'Football', ''),
(135, 'esa5rwte', 1, 'Cricket', '', ''),
(136, 'rqwrwqr', 1, 'Cricket', '', 'Tanis'),
(137, 'rwteetet', 1, 'Cricket', '', ''),
(138, 'et4eryut', 1, '', 'Football', ''),
(139, '4eryt654r7y5r7y5', 1, 'Cricket', '', 'Tanis'),
(140, '54r76y57y57y', 1, '', 'Football', ''),
(141, '465iu6yu', 1, 'Cricket', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE IF NOT EXISTS `profilepicture` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `paths` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`id`, `name`, `paths`) VALUES
(12, 'PHP Soldier', 'Uploads/pic.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `summarys`
--

CREATE TABLE IF NOT EXISTS `summarys` (
`id` int(11) NOT NULL,
  `author` varchar(255) NOT NULL,
  `summ` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summarys`
--

INSERT INTO `summarys` (`id`, `author`, `summ`) VALUES
(13, 'BASIS-BITM', 'PHP Soldier&nbsp;    \r\n                                '),
(14, 'IT BAZER', 'This is my company<br><br>    \r\n                                '),
(16, 'It World', 'this is a it world    \r\n                                ');

-- --------------------------------------------------------

--
-- Table structure for table `typeofgender`
--

CREATE TABLE IF NOT EXISTS `typeofgender` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `typeofgender`
--

INSERT INTO `typeofgender` (`id`, `name`, `gender`) VALUES
(7, 'Kowsare Mahmud', 'Male'),
(9, 'Miss Oliur Rahaman', 'Female'),
(10, 'Soikot Bai', 'Male'),
(11, 'Shahin Bai', 'Male');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookmark`
--
ALTER TABLE `bookmark`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `citys`
--
ALTER TABLE `citys`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepicture`
--
ALTER TABLE `profilepicture`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summarys`
--
ALTER TABLE `summarys`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typeofgender`
--
ALTER TABLE `typeofgender`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `bookmark`
--
ALTER TABLE `bookmark`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `citys`
--
ALTER TABLE `citys`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=142;
--
-- AUTO_INCREMENT for table `profilepicture`
--
ALTER TABLE `profilepicture`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `summarys`
--
ALTER TABLE `summarys`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `typeofgender`
--
ALTER TABLE `typeofgender`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
