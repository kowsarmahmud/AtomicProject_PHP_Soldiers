<?php
//include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'AtomicProject_PHP_Soldiers' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php'); //using absolute path

//defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
//defined('SITE_ROOT') ? null : define('SITE_ROOT', 'C:' . DS . 'xampp' . DS . 'htdocs' . DS . 'AtomicProject_PHP_Soldiers');
//require_once(SITE_ROOT . DS . "vendor/autoload.php");
include '../../startup.php';

use App\Bitm\PHP_Soldiers\File\Picture;
use App\BITM\PHP_Soldiers\Utility\Utility;

$picture = new Picture();
//echo "<br>";
$results = $picture->index();

// $hobby->create();
?>  
<?php include 'layout/header.php'; ?>

<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.php">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">Tables</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Profile Picture</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>Well done!</strong><h3><?= Utility::message(); ?></h3>
                </div>
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Name</th>
                            <th>Profile Picture</th>
                            <th>Status</th>
                            <th>Actions <a style="float: right; margin-right: 10px; color: green;"title="Download" href="#"> Download as PDF | XL</a></th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        $num = 1;
                        foreach ($results as $result) {
                            ?>
                            <tr>
                                <td><?php echo $num; ?></td>
                                <td><?= $result->name; ?></td>
                                <td><img src="<?= $result->paths; ?>" alt="HTML5 Icon" width="138" height="128"></td>
                                <td class="center">
                                    <span class="label label-success">Active</span>
                                </td>
                                <td class="center">
                                    <a class="btn btn-success" title="View" href="view.php?id=<?= $result->id; ?>">
                                        <i class="halflings-icon white zoom-in"></i>  
                                    </a>
                                    <a class="btn btn-info" title="Edit" href="edit.php?id=<?= $result->id; ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>
                                    <a class="btn btn-danger" title="Delete" href="delete.php?id=<?= $result->id; ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                    <a class="btn btn-setting" title="Trash/Recover" href="trash.php">
                                        <i class="halflings-icon white chevron-up"></i> 
                                    </a>
                                    <a style="background-color: #FF9100;"class="btn" title="Email to Friend" href="email.php">
                                        <i class="halflings-icon white e-mail"></i> 
                                    </a>
                                </td>
                            </tr>
                            <?php
                            ++$num;
                        }
                        ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->
    <a class="btn btn-success" href="create.php">Add Profile Picture</a>





</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->

<?php include 'layout/footer.php'; ?>