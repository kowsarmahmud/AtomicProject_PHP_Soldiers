<!DOCTYPE html>
<html>
    <head>
        <title>AtomicProject_Kowsar_106275</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>BITM - Web App Dev - PHP</h1>
        <dl>
            <dt><b>Name:</b></dt>
            <dd>BITM - BASIS</dd>

            <dt><b>Group Name:</b> </dt>
            <dd>PHP_Soldiers</dd>

            <dt><b>Batch:</b></dt>
            <dd>B-11</dd>
        </dl>
        <h2>AtomicProject_PHP-Soldiers</h2>
        <table border="1" style="padding: 15px">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>Project Name</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>01</td>
                    <td><a href="views/PHP_Soldiers/Checkbox/index.php">Favourite Hobby</a></td>
                </tr>
                <tr>
                    <td>02</td>
                    <td><a href="views/PHP_Soldiers/Date/index.php">Birthday</a></td>
                </tr>
                <tr>
                    <td>03</td>
                    <td><a href="views/PHP_Soldiers/Email/index.php">Email Subscription</a></td>
                </tr>
                <tr>
                    <td>04</td>
                    <td><a href="views/PHP_Soldiers/Textarea/index.php">Summary of Organizations</a></td>
                </tr>
                <tr>
                    <td>05</td>
                    <td><a href="views/PHP_Soldiers/Radio/index.php">Gender</a></td>
                </tr>
                <tr>
                    <td>06</td>
                    <td><a href="views/PHP_Soldiers/Select/index.php">Favourite City</a></td>
                </tr>
                <tr>
                    <td>07</td>
                    <td><a href="views/PHP_Soldiers/Text/index.php">Favourite Books</a></td>
                </tr>
                <tr>
                    <td>08</td>
                    <td><a href="views/PHP_Soldiers/File/index.php">Profile Picture</a></td>
                </tr>
                <tr>
                    <td>09</td>
                    <td><a href="views/PHP_Soldiers/Textareamultiple/index.php">Bookmarking</a></td>
                </tr>
            </tbody>
        </table>

    </body>
</html>
