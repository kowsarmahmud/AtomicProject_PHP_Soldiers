<?php

namespace App\Bitm\PHP_Soldiers\Select;
use App\BITM\PHP_Soldiers\Utility\Utility;

class City {

    public $table = "citys";
    public $id;
    public $city;
    public $country;
    public $connection;

    public function __construct($data = false) {
        $this->id = $data['id'];
        $this->city = $data['city'];
        $this->country = $data['country'];
        
        $this->open_connection();
    }

    public function open_connection() {
        $this->connection = mysql_connect("localhost", "root", "");
        if (!$this->connection) {
            die("Database connection failed: " . mysql_error());
        } else {
            $db_select = mysql_select_db("atomicproject", $this->connection);
            if (!$db_select) {
                die("Database selection failed: " . mysql_error());
            }
        }
    }

    public function close_connection() {
        if (isset($this->connection)) {
            mysql_close($this->connection);
            unset($this->connection);
        }
    }

    public function index() {

        $hobbys = array();

        $query = "SELECT * FROM " . $this->table . " ORDER BY id DESC";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $hobbys[] = $row;
        }
        return $hobbys;
    }

    public function store() {

        $query = "INSERT INTO " . $this->table . " ( `country`,`city`) VALUES ( '" . $this->country . "','" . $this->city . "')";
        $result = mysql_query($query);

        if ($result) {
            $message = "<h1>Hobby title is added successfully.</h1>";
            header('Location:index.php');
        } else {
            $message = "<h1>There is an error while saving data. Please try again later.</h1>";
            Utility::message($message);
        }
    }

    public function get_single_id_details($id = NULL) {

        $query = "SELECT * FROM " . $this->table . " WHERE id='$id' ";
        $result = mysql_query($query);

        return $result;
    }

    public function update_single_id_details() {

        $query = "UPDATE " . $this->table . " SET `country` = '" . $this->country . "',`city` = '" . $this->city . "' WHERE `" . $this->table . "`.`id` = " . $this->id . "";
        if (mysql_query($query)) {
            return true;
        } else {
            die('Query problem' . mysql_error());
        }
    }

    public function delete($id = NULL) {

        $query = "DELETE FROM " . $this->table . " WHERE `" . $this->table . "`.`id` = '$id'";

        if (mysql_query($query)) {
            header('Location:index.php');
        } else {
            die('Query problem' . mysql_error());
        }
    }

    public function published($id = NULL) {

        $quary = "UPDATE `" . $this->table . "` SET `deletion_status` = '1' WHERE `id` = '$id .'";
        mysql_query($quary);
        header('Location:index.php');
    }

    public function unpublished($id = NULL) {

        $quary = "UPDATE `" . $this->table . "` SET `deletion_status` = '0' WHERE `id` = '$id .'";
        mysql_query($quary);
        header('Location:index.php');
    }

}
