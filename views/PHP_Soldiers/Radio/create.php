<?php
//include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'AtomicProject_PHP_Soldiers' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php'); //using absolute path

defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
defined('SITE_ROOT') ? null : define('SITE_ROOT', 'C:' . DS . 'xampp' . DS . 'htdocs' . DS . 'AtomicProject_PHP_Soldiers');
require_once(SITE_ROOT . DS . "vendor/autoload.php");

use App\Bitm\PHP_Soldiers\Radio\Gender;

$gender = new Gender();
//echo "<br>";
//$hobby->delete();
// $hobby->create();
?>  

<?php include 'layout/header.php'; ?>

<!-- start: Content -->
<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="list.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Forms</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Gender</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form action="store.php" method="POST" class="form-horizontal">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="bookmark"> Name</label>
                            <div class="controls">
                                <input class="input-xlarge focused"
                                       autofocus="autofocus" 
                                       id="bookmark" 
                                       type="text" 
                                       name="name"
                                       tabindex="1"
                                       placeholder="input gender name"
                                       required="required" >
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Gender</label>
                            <div class="controls">
                                <label class="radio">
                                    <input type="radio" name="gender" id="optionsRadios1" value="Male" checked="">
                                    Male
                                </label>
                                <div style="clear:both"></div>
                                <label class="radio">
                                    <input type="radio" name="gender" id="optionsRadios2" value="Female">
                                    Female
                                </label>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" tabindex="2" class="btn btn-primary">Save</button>
                            <button type="submit" tabindex="3" class="btn btn-primary">Save & Add Again</button>
                            <input tabindex="3" tabindex="4" class="btn" type="reset" value="Reset" />
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->
        <a class="btn btn-success" href="index.php">Go to List</a>
        <a class="btn btn-success" href="javascript:history.go(-1)">Back</a>
        <h1><?php //$hobby->create();  ?></h1>

    </div><!--/row-->
</div><!--/.fluid-container-->
<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->

<?php include 'layout/footer.php'; ?>
