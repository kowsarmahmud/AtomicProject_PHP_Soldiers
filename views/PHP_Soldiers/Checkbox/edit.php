<?php
//include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'AtomicProject_PHP_Soldiers' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php'); //using absolute path

//defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
//defined('SITE_ROOT') ? null : define('SITE_ROOT', 'C:' . DS . 'xampp' . DS . 'htdocs' . DS . 'AtomicProject_PHP_Soldiers');
//require_once(SITE_ROOT . DS . "vendor/autoload.php");
include '../../startup.php';

use App\Bitm\PHP_Soldiers\Checkbox\Hobby;

$hobby = new Hobby();

//$id = $_GET['id'];
$single_id = $hobby->get_single_id_details($_GET['id']);
$single = mysql_fetch_assoc($single_id);
//print_r($single_id);
//exit();
if (isset($_POST['submit'])) {
 
    $hobby = new Hobby($_POST);
    
    $result =  $hobby->update_single_id_details();
    
    if ($result == true) {
        header('Location:index.php');
    }
}
?>  
<?php include 'layout/header.php'; ?>

<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="list.php">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Forms</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Favourite Hobby</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form action="" method="POST" class="form-horizontal">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="hobby">Edit Name</label>
                            <div class="controls">
                                <input class="input-xlarge focused" 
                                       id="focusedInput" 
                                       type="text"                                        
                                       autofocus="autofocus" 
                                       id="bookmark" 
                                       type="text" 
                                       value="<?php echo $single['title']; ?>"
                                       name="title"
                                       tabindex="1"
                                       placeholder="edit hobby name"
                                       required="required" >

                                <input class="input-xlarge focused"
                                       id="hobby"
                                       type="hidden" 
                                       value="<?php echo $single['id']; ?>"
                                       name="id"
                                       >
                            </div>                           
                        </div>
                        <div class="control-group">
                            <label class="control-label">Your Favourite Hobby</label>
                            <div class="controls">
                                <label class="checkbox inline">
                                    <input type="checkbox" name="cricket" id="inlineCheckbox1" value="<?php echo $single['cricket']; ?>"> Cricket
                                </label>
                                <label class="checkbox inline">
                                    <input type="checkbox" name="football" id="inlineCheckbox2" value="<?php echo $single['football']; ?>"> Football
                                </label>
                                <label class="checkbox inline">
                                    <input type="checkbox" name="tanis" id="inlineCheckbox3" value="<?php echo $single['tanis']; ?>"> Tanis
                                </label>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" tabindex="2" class="btn btn-primary" name="submit" value="submit">Update</button>
                            <button type="reset" tabindex="3" class="btn">Cancel</button>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->
        <a class="btn btn-success" href="index.php">Go to List</a>
        <a class="btn btn-success" href="javascript:history.go(-1)">Back</a>
        <h1><?php //$hobby->edit();   ?></h1>
    </div><!--/row-->






</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->

<?php include 'layout/footer.php'; ?>