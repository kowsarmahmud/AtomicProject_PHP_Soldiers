<?php
//include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'AtomicProject_PHP_Soldiers' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php'); //using absolute path

defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
defined('SITE_ROOT') ? null : define('SITE_ROOT', 'C:' . DS . 'xampp' . DS . 'htdocs' . DS . 'AtomicProject_PHP_Soldiers');
require_once(SITE_ROOT . DS . "vendor/autoload.php");

use App\Bitm\PHP_Soldiers\File\Picture;

$picture = new Picture();
$single_id = $picture->get_single_id_details($_GET['id']);
$single = mysql_fetch_assoc($single_id);
?>  
<?php include 'layout/header.php'; ?>

<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="list.php">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">View</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Profile Picture</h2>

            </div>
            <div class="box-content">
                <form class="form-horizontal">
                    <fieldset>
                        <h3><?php echo $single['name']; ?></h3>
                        <h4><img src="<?php echo $single['paths']; ?>" alt="HTML5 Icon" width="138" height="128"></h4>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

    <a class="btn btn-success" href="index.php">Go to List</a>
    <a class="btn btn-success" href="javascript:history.go(-1)">Back</a>




</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->


<?php include 'layout/footer.php'; ?>