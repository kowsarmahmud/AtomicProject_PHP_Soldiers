<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- start: Meta -->
        <meta charset="utf-8">
        <title>Birthday</title>
        <meta name="description" content="Bootstrap Metro Dashboard">
        <meta name="author" content="Dennis Ji">
        <meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
        <!-- end: Meta -->

        <!-- start: Mobile Specific -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- end: Mobile Specific -->

        <!-- start: CSS -->
        <link id="bootstrap-style" href="./Resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="./Resource/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link id="base-style" href="./Resource/bootstrap/css/style.css" rel="stylesheet">
        <link id="base-style-responsive" href="./Resource/bootstrap/css/style-responsive.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
        <!-- end: CSS -->


        <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                <link id="ie-style" href="css/ie.css" rel="stylesheet">
        <![endif]-->

        <!--[if IE 9]>
                <link id="ie9style" href="css/ie9.css" rel="stylesheet">
        <![endif]-->

        <!-- start: Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico">
        <!-- end: Favicon -->

        <style type="text/css">
            body { background: url(./Resource/bootstrap/img/bg-login.jpg) !important; }
        </style>



    </head>

    <body>
        <div class="container-fluid-full">
            <div class="row-fluid">

                <h1>BITM - Web App Dev - PHP</h1>
        <dl>
            <dt><span>Name:</span></dt>
            <dd>BITM - BASIS</dd>

            <dt>SEIP ID:</dt>
            <dd>SEIP106275</dd>

            <dt>Batch:</dt>
            <dd>11</dd>
        </dl>
        <h2>Projects</h2>
        <table>
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>Project Name</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>01</td>
                    <td><a href="views/PHP_Soldiers/Checkbox/index.php">Favourite Hobby</a></td>
                </tr>
                <tr>
                    <td>02</td>
                    <td><a href="./Views/SEIP106275/Textmultiple/index.php">Mobile Models</a></td>
                </tr>
                <tr>
                    <td>03</td>
                    <td><a href="views/PHP_Soldiers/Email/index.php">Email Subscription</a></td>
                </tr>
                <tr>
                    <td>04</td>
                    <td><a href="views/PHP_Soldiers/File/index.php">Profile Picture</a></td>
                </tr>
                <tr>
                    <td>05</td>
                    <td><a href="views/PHP_Soldiers/Radio/index.php">Gender</a></td>
                </tr>
                <tr>
                    <td>06</td>
                    <td><a href="views/PHP_Soldiers/Select/index.php">Favourite City</a></td>
                </tr>
                <tr>
                    <td>07</td>
                    <td><a href="views/PHP_Soldiers/Text/index.php">Favourite Books</a></td>
                </tr>
                <tr>
                    <td>08</td>
                    <td><a href="views/PHP_Soldiers/Textarea/index.php">Summary of Organizations</a></td>
                </tr>
                <tr>
                    <td>09</td>
                    <td><a href="views/PHP_Soldiers/Textareamultiple/index.php">Bookmarking</a></td>
                </tr>
                <tr>
                    <td>10</td>
                    <td><a href="views/PHP_Soldiers/Date/index.php">Birthday</a></td>
                </tr>
            </tbody>
        </table><!--/row-->


            </div><!--/.fluid-container-->

        </div><!--/fluid-row-->

        <!-- start: JavaScript-->

        <script src="./Resource/bootstrap/js/jquery-1.9.1.min.js"></script>
        <script src="./Resource/bootstrap/js/jquery-migrate-1.0.0.min.js"></script>

        <script src="./Resource/bootstrap/js/jquery-ui-1.10.0.custom.min.js"></script>

        <script src="./Resource/bootstrap/js/jquery.ui.touch-punch.js"></script>

        <script src="./Resource/bootstrap/js/modernizr.js"></script>

        <script src="./Resource/bootstrap/js/bootstrap.min.js"></script>

        <script src="./Resource/bootstrap/js/jquery.cookie.js"></script>

        <script src='./Resource/bootstrap/js/fullcalendar.min.js'></script>

        <script src='./Resource/bootstrap/js/jquery.dataTables.min.js'></script>

        <script src="./Resource/bootstrap/js/excanvas.js"></script>
        <script src="./Resource/bootstrap/js/jquery.flot.js"></script>
        <script src="./Resource/bootstrap/js/jquery.flot.pie.js"></script>
        <script src="./Resource/bootstrap/js/jquery.flot.stack.js"></script>
        <script src="./Resource/bootstrap/js/jquery.flot.resize.min.js"></script>

        <script src="./Resource/bootstrap/js/jquery.chosen.min.js"></script>

        <script src="./Resource/bootstrap/js/jquery.uniform.min.js"></script>

        <script src="./Resource/bootstrap/js/jquery.cleditor.min.js"></script>
        <script src="./Resource/bootstrap/js/jquery.noty.js"></script>

        <script src="./Resource/bootstrap/js/jquery.elfinder.min.js"></script>

        <script src="./Resource/bootstrap/js/jquery.raty.min.js"></script>

        <script src="./Resource/bootstrap/js/jquery.iphone.toggle.js"></script>

        <script src="./Resource/bootstrap/js/jquery.uploadify-3.1.min.js"></script>

        <script src="./Resource/bootstrap/js/jquery.gritter.min.js"></script>

        <script src="./Resource/bootstrap/js/jquery.imagesloaded.js"></script>

        <script src="./Resource/bootstrap/js/jquery.masonry.min.js"></script>

        <script src="./Resource/bootstrap/js/jquery.knob.modified.js"></script>

        <script src="./Resource/bootstrap/js/jquery.sparkline.min.js"></script>

        <script src="./Resource/bootstrap/js/counter.js"></script>

        <script src="./Resource/bootstrap/js/retina.js"></script>

        <script src="./Resource/bootstrap/js/custom.js"></script>
        <!-- end: JavaScript-->

    </body>
</html>
